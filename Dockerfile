FROM rocker/tensorflow
ENV MINIMAP2_VERSION 2.17
ENV SAMTOOLS_VERSION 1.10
ENV HTSLIB_VERSION 1.10.2

MAINTAINER Mattia Furlan <mattia.furlan@iit.it>

RUN export DEBIAN_FRONTEND=noninteractive \
	&& apt-get update && \
	apt-get -y install \
		libbz2-dev \
		liblzma-dev \
		bzip2 \
		autoconf \
		wget


RUN wget -O "environmentSetUp.R" "https://gitlab.com/mfurla/nanoid/-/raw/master/environmentSetUp.R?inline=false" \
	&& Rscript "environmentSetUp.R"

RUN wget -O /minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2 "https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2" \
	&& tar -jxf /minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2 \
	&& cp /minimap2-${MINIMAP2_VERSION}_x64-linux/minimap2 /usr/local/bin

RUN git clone https://github.com/samtools/htslib.git \
	&& cd /htslib \
	&& git checkout ${HTSLIB_VERSION}

RUN git clone https://github.com/samtools/samtools.git \
	&& cd /samtools \
	&& git checkout ${SAMTOOLS_VERSION} \
	&& autoreconf \
	&& ./configure --without-curses --with-htslib=../htslib \
	&& make \
	&& make install
