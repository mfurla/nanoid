### create sequencing summary matrices ###
sequencing.files = list.files("data",pattern = "sequencing_summary",full.names = TRUE,recursive = TRUE)

sequencing.summary <- read.delim(sequencing.files[[1]],sep="\t",row.names = NULL,header = TRUE,stringsAsFactors = FALSE)

if(length(sequencing.files>1))
{
	for(sequencing.file in sequencing.files[-1])
	{
		sequencing.summary <- rbind(sequencing.summary,read.delim(sequencing.file,sep="\t",row.names = NULL,header = TRUE,stringsAsFactors = FALSE))
	}
}

rownames(sequencing.summary) = sequencing.summary[,"read_id"]

save(sequencing.summary,file=file.path("data","sequencing.summary.RData"))